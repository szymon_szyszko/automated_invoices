import requests
import json
import pandas as pd
from datetime import datetime

# Open config file to get API key and URL
with open("config.json") as file:
    config = json.load(file)
    apiKey = config["API_KEY"]
    apiUrl = config["API_URL"]
headers = {"Authorization": apiKey}
turn_v_to_yes_in_output = {"v": "Yes",
                           "": "No"}
EXPECTED_COLUMN_NAMES = ["Project", "Task", "Description", "Person", "Time spent", "Rate", "Cost"]


def get_time_info(board_id, item_id):
    """
    Function which returns the time information and user IDs of a specific task

    board_id: (str) ID string of numbers for the company name
    item_id: (str) ID string of numbers for a specific task

    return: time_spent_dict: (dict) time spent on each task by each person and user IDs for each person
    """
    time_spent_dict = {}
    query = '{boards (ids:' + str(board_id) + ') {items(ids:' + str(item_id) + ')\
{column_values(ids:"time_tracking") { value  }}}}'
    data = {'query': query}
    r = requests.post(url=apiUrl, json=data, headers=headers)  # make request
    api_query_data = r.json()
    column_values = api_query_data['data']['boards'][0]['items'][0]['column_values']
    # if functions to check there is data to extract
    if not column_values:
        user_ids = ["null"]
        time_spent = ["null"]
        time_spent_dict["null"] = {"time": "null"}
        return
    if column_values[0] is None:
        user_ids = ["null"]
        time_spent = ["null"]
        time_spent_dict["null"] = {"time": "null"}
        return
    # sets s to the string containing the time data
    s = column_values[0]['value']
    # if function to check s contains data
    if s is None:
        user_ids = ["null"]
        time_spent = ["null"]
        time_spent_dict["null"] = {"time": "null"}
        return
    # converts the string to a json file so time information and user IDs can be easily read
    time_sheet_data = json.loads(s)
    # Lists for IDs and time values, could be changed to single dictionary including both values
    user_ids = []
    time_spent = []
    # Checks the json contains the "additional values" section which contains the user IDs and time data
    if "additional_value" not in time_sheet_data:
        user_ids = ["null"]
        time_spent = ["null"]
        time_spent_dict["null"] = {"time": "null"}
        return
    additional_column_values = time_sheet_data["additional_value"]
    for n in additional_column_values:
        # Reads start time from the json, stores it in datetime format
        start_time = datetime.strptime(n['started_at'], '%Y-%m-%d %H:%M:%S %Z')
        # If no "ended at" the time tracking is in use
        if n['ended_at']:
            end_time = datetime.strptime(n['ended_at'], '%Y-%m-%d %H:%M:%S %Z')
        else:
            # Error message for if time recording is being used, may not work as hard to test
            print(f"Error: Monday time monitoring in use.")
            end_time = start_time
        delta = end_time - start_time
        charge_time = get_charge_time(delta)
        if not n['started_user_id'] in user_ids:
            user_ids.append(n['started_user_id'])
            time_spent.append(delta)
            time_spent_dict[n['started_user_id']] = {"time": charge_time}
        # If the user has used time recording before, the time is added onto the time associated with that user ID
        elif n['started_user_id'] in user_ids:
            index = user_ids.index(n['started_user_id'])
            time_spent[index] = time_spent[index] + delta
            time_spent_dict[n['started_user_id']]["time"] = time_spent_dict[n['started_user_id']]["time"] + charge_time
    return time_spent_dict


def create_invoice(board_name, only_support):
    """
    Function for checking if there is a "invoiced", "support" and "chargeable" section, also checks if only the support
    items are to be output

    board_name: (str) name of the company the invoice is for
    only_support: (bool) if only support tasks are to be output, set by user
    """
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Creating invoice.")
    board_data, item_dict, has_support, has_invoiced, has_chargeable = get_board_project_id(board_name)
    with open('user_mappings.json') as user_mapping:
        userids = json.load(user_mapping)
        userids = userids[board_name]
    project_index = 0
    # Checks if there is a support/invoiced/chargeable column
    if has_support and has_invoiced and has_chargeable:
        # Check if only support data is required
        if only_support:
            only_support_data_out(board_name, userids, board_data, item_dict)
        if not only_support:
            not_only_support_data_out(board_name, userids, board_data, item_dict)
    else:
        # If no support/invoiced/chargeable column all data is output
        now = datetime.now()
        time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
        print(f"{time_stamp}    No support/invoiced/chargeable data, printing all.")
        all_data_out(board_name, userids, board_data, item_dict)
    user_mapping.close()


def get_board_project_id(board_name):
    """
    Function for getting details about each task within a board, makes a api request for details about each task

    board_name: (str) name of the company the invoice is for

    return:
        board_name_ids: (dict) contains the name of the board (company) and the ID for that board
        item_dict (dict): contains the data gathered on each specific task (group (project), project name (task),
        support, chargeable, invoiced, description)
        has_support: (bool) True if there is a support column for this item on monday
        has_invoiced: (bool) True if there is an invoiced column for this item on monday
        has_chargeable: (bool) True if there is a chargeable column for this item on monday
    """
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Getting board and project IDs.")
    query = '{boards { name id description items { name id group {title} column_values{title text } } } }'
    data = {'query': query}
    # Create query to get board names and project IDs
    r = requests.post(url=apiUrl, json=data, headers=headers)  # make request
    api_data = r.json()
    item_dict = {}
    has_support = has_invoiced = has_chargeable = False
    # create dictionary for all task information
    board_names_ids = {}
    for x in api_data['data']['boards']:
        if x['name'] == board_name:
            board_names_ids[x['name']] = x['id']
            for y in x['items']:
                column_values_dict = check_column_values(y['column_values'])
                group_name = y['group']['title']
                project_name = y['name']
                has_support = column_values_dict['Has Support']
                has_invoiced = column_values_dict['Has Invoiced']
                has_chargeable = column_values_dict['Has Chargeable']
                item_dict[y['id']] = {"group": group_name,
                                      "project name": project_name,
                                      "support": column_values_dict['Is Support'],
                                      "chargeable": column_values_dict['Is Chargeable'],
                                      "invoiced": column_values_dict['Is invoiced'],
                                      "description": column_values_dict['Description']}
    return board_names_ids, item_dict, has_support, has_invoiced, has_chargeable


def check_column_values(data_values):
    """
    Function to check if there is a support/invoiced/chargeable column and extract data stored within the columns

    data_values: (list) contains a list of dicts, each with information about the task

    return: column_values_dict: (dict) contains data about the task contained within the columns in monday
    """
    is_support = is_chargeable = is_invoiced = description = ''
    has_support = has_invoiced = has_chargeable = False
    for x in data_values:
        if x['title'] == 'Is Support':
            is_support = x['text']
            has_support = True
        if x['title'] == 'Is invoiced':
            is_invoiced = x['text']
            has_invoiced = True
        if x['title'] == 'Chargable':
            is_chargeable = x['text']
            has_chargeable = True
        if x['title'] == 'Description':
            description = x['text']
    column_values_dict = {'Is Support': is_support,
                          'Has Support': has_support,
                          'Is invoiced': is_invoiced,
                          'Has Invoiced': has_invoiced,
                          'Is Chargeable': is_chargeable,
                          'Has Chargeable': has_chargeable,
                          'Description': description}
    return column_values_dict


def only_support_data_out(board_name, user_id_dict, board_name_ids, item_dict):
    """
    Function to output only support tasks that are chargeable but not invoiced

    board_name (str): name of the company the invoice is being made for
    user_id_dict (dict): a dictionary of the data stored in user_mappings.json, has the id, name and rate of each
    person
    board_name_ids (dict): contains the name and id of the "board" (company) the invoice is being made for
    item_dict (dict): contains the data gathered on each specific task (group (project), project name (task), support,
    chargeable, invoiced, description)
    """
    # Names the output file
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d_%H-%M-%S')
    out_filename = f"{board_name}_Monday_Timesheet_{time_stamp}.csv"
    column_names = EXPECTED_COLUMN_NAMES
    main_data_frame = pd.DataFrame(columns=column_names)
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Transforming data into spreadsheet.")
    for x in item_dict:
        if not x:
            print("No data")
            return
        item_id = x
        time_info_dict = get_time_info(board_name_ids[board_name], x)
        if time_info_dict:
            for user in time_info_dict:
                rate = user_id_dict[str(user)]["rate"]
                time = time_info_dict[user]["time"]
                cost = round(time * rate, 2)
                project = item_dict[x]["group"]
                task = item_dict[x]["project name"]
                description = item_dict[x]["description"]
                person = user_id_dict[str(user)]["name"]
                # Checks if the item is support, chargeable and not invoiced
                if item_dict[x]["support"] == "v" and item_dict[x]["invoiced"] != "v" \
                        and item_dict[x]["chargeable"] == "v":
                    sub_data_frame = pd.DataFrame({"Project": project,
                                                   "Task": task,
                                                   "Description": description,
                                                   "Person": person,
                                                   "Time spent": round(time, 2),
                                                   "Rate": rate,
                                                   "Cost": cost},
                                                  index=[0])
                    main_data_frame = main_data_frame.append(sub_data_frame, ignore_index=True)
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Finished Successfully.")
    main_data_frame.to_csv(out_filename, index=False, line_terminator='\n')


def not_only_support_data_out(board_name, user_id_dict, board_name_ids, item_dict):
    """
    Function to output all tasks that are chargeable but not invoiced

    board_name (str): name of the company the invoice is being made for
    user_id_dict (dict): a dictionary of the data stored in user_mappings.json, has the id, name and rate of each
    person
    board_name_ids (dict): contains the name and id of the "board" (company) the invoice is being made for
    item_dict (dict): contains the data gathered on each specific task (group (project), project name (task), support,
    chargeable, invoiced, description)
    """
    # Names the output file
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d_%H-%M-%S')
    out_filename = f"{board_name}_Monday_Timesheet_{time_stamp}.csv"
    column_names = ["Project", "Task", "Description", "Person", "Time spent", "Is Support", "Rate", "Cost"]
    main_data_frame = pd.DataFrame(columns=column_names)
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Transforming data into spreadsheet.")
    for x in item_dict:
        if not x:
            print("No data")
            return
        item_id = x
        time_info_dict = get_time_info(board_name_ids[board_name], x)
        if time_info_dict:
            for user in time_info_dict:
                rate = user_id_dict[str(user)]["rate"]
                time = time_info_dict[user]["time"]
                cost = round(time * rate, 2)
                project = item_dict[x]["group"]
                task = item_dict[x]["project name"]
                description = item_dict[x]["description"]
                person = user_id_dict[str(user)]["name"]
                support = turn_v_to_yes_in_output[item_dict[x]["support"]]
                # Checks if the item is support, chargeable and not invoiced
                if item_dict[x]["invoiced"] != "v" and item_dict[x]["chargeable"] == "v":
                    sub_data_frame = pd.DataFrame({"Project": project,
                                                   "Task": task,
                                                   "Description": description,
                                                   "Person": person,
                                                   "Time spent": round(time, 2),
                                                   "Is Support": support,
                                                   "Rate": rate,
                                                   "Cost": cost},
                                                  index=[0])
                    main_data_frame = main_data_frame.append(sub_data_frame, ignore_index=True)
    main_data_frame.to_csv(out_filename, index=False, line_terminator='\n')


def all_data_out(board_name, user_id_dict, board_name_ids, item_dict):
    """
    Output function for if there is no support/chargeable/invoiced column on monday


    board_name (str): name of the company the invoice is being made for
    user_id_dict (dict): a dictionary of the data stored in user_mappings.json, has the id, name and rate of each
    person
    board_name_ids (dict): contains the name and id of the "board" (company) the invoice is being made for
    item_dict (dict): contains the data gathered on each specific task (group (project), project name (task), support,
    chargeable, invoiced, description)
    """
    # Names the output file
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d_%H-%M-%S')
    out_filename = f"{board_name}_Monday_Timesheet_{time_stamp}.csv"
    column_names = EXPECTED_COLUMN_NAMES
    main_data_frame = pd.DataFrame(columns=column_names)
    now = datetime.now()
    time_stamp = now.strftime('%Y-%m-%d %H:%M:%S')
    print(f"{time_stamp}    Transforming data into spreadsheet.")
    for x in item_dict:
        if not x:
            print("No data")
            return
        item_id = x
        time_info_dict = get_time_info(board_name_ids[board_name], x)
        if time_info_dict:
            for user in time_info_dict:
                rate = user_id_dict[str(user)]["rate"]
                time = time_info_dict[user]["time"]
                cost = round(time * rate, 2)
                project = item_dict[x]["group"]
                task = item_dict[x]["project name"]
                description = item_dict[x]["description"]
                person = user_id_dict[str(user)]["name"]
                sub_data_frame = pd.DataFrame({"Project": project,
                                               "Task": task,
                                               "Description": description,
                                               "Person": person,
                                               "Time spent": round(time, 2),
                                               "Rate": rate,
                                               "Cost": cost},
                                              index=[0])
                main_data_frame = main_data_frame.append(sub_data_frame, ignore_index=True)
    main_data_frame.to_csv(out_filename, index=False, line_terminator='\n')


def get_charge_time(delta):
    """
    Function for turning time spent into hours

    delta (timedelta): the difference between the 'started at' and 'ended at' times

    return: charge_time: (float) the amount of time spent on a task by a specific person in hours
    """
    days, seconds = delta.days, delta.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    # Turns the time spent into hours
    charge_time = float(hours) + minutes / float(60) + seconds / float(3600)
    # Checks if it's the first time recording from that user
    return charge_time


# Input
create_invoice("Quietroom", only_support=False)
