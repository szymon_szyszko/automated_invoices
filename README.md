# README #


This code is for creating invoices from data stored on Monday.com. The output is a csv sile containing the total time spent and cost of each task.
This uses the following modules; json, pandas, datetime


### How to set up ###

A config.json file is needed to run this code. This file should include an API key and URL. It should be in the format of:

{"API_KEY": "Put your API key here",
  "API_URL": "Put your URL here"
}

The rates for Quiertroom also need to be changed



### How to run ###
To run this, the name of the project should be changed at the bottom of the file to the desired project in the create_invoice function call. The support_only should be changed too true or false depending on if only the support tasks are required.
